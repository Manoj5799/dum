import React, {Component} from "react";
class Questions extends Component{
    state={
        click:0
    }
    change = (an,alpha) => {
       this.props.change(an,alpha)
    }
    
    
    render() {
        let {name,qus,ans,change, questions,allAnswer,random} = this.props;
        let {click} = this.state;
        
        console.log(this.props.qus)
        return (
            <div classname="a2">
            <React.Fragment>
            <h2 className="h1">Welcome to the Picture Quiz : {name} </h2>
            <div className="containers1">     
           {/* <h5 className="w1">Question No : {qus+1}</h5> */}
             <img className="img1" src={questions[random].link} /><br/>
             <div className="btnn">
            <button className="btn btn-dark m-2" onClick={() =>change(questions[random].options[0],qus,0,random)}>A : {questions[random].options[0]}</button>
            <button className="btn btn-dark m-2"  onClick={() =>change(questions[random].options[1],qus,1,random)}>B : {questions[random].options[1]}</button>
            <button className="btn btn-dark m-2" onClick={() =>change(questions[random].options[2],qus,2,random)}>C : {questions[random].options[2]}</button><br/>
            <h3 className="text text-danger">{this.props.error}</h3>
             </div>
             </div>
            </React.Fragment>
            </div>
        );
    }
    
}
export default Questions;