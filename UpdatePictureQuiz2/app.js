let express=require('express');
require("dotenv").config();
let app=express();
app.use(express.json());
const {Client}=require("pg");
const client = new Client({
user : process.env.API_USER,
password : process.env.API_PASSWORD,
database: process.env.API_DATABASE,
//port: 5432,
host: process.env.API_HOST,
ssl: { rejectUnauthorized: false},

})
client.connect(function(res, error) {
    console.log(`Connected!!!`)
});
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTIONS, PUT, PATCH,DELETE,HEAD"
    );
    res.header(
     "Access-Control-Allow-Headers",
     "Origin, X-Requested-with, Content-Type,Accept"
    );
    next();
});


var port =   process.env.PORT || 2410;
app.listen(port, () => console.log(`Node app listening on port ${port}!`));


app.get("/hey", function(req,res,next){
    console.log("hey");
    res.send("Hey");

});

app.get("/players", function(req,res,next){
    console.log("Inside /players get api");
    const query = "SELECT * FROM players";
    client.query(query,function(err,result){
        if(err){
            res.send(404).send(err);
        }
    res.send(result);
   // client.end();
})
})

app.post("/players", function(req, res,next) {
    var values=Object.values(req.body);
    console.log(values);
    let query= "INSERT INTO players (playCode,name,fname) VALUES ($1,$2,$3)";
    client.query(query,values,function(err,result){
        if(err) {
            res.status(404).send(err);
        }
        res.send(result)
        });  
    });

