import React, { Component } from "react";
class AddProduct extends Component {
  state = {
    product: this.props.product,
    errors: {},
    
  };
  validateCode = (code) => (!code ? ("Product Code is mandaotry") : "");
  validatePrice = (price) => (!price ? "Product Price is mandaotry" : "");
  validateCategory = (category) => (!category ? alert("Category is mandaotry") : "");
  validateBrand = (brand,category) => (category ? !brand ? alert("Brand is mandaotry") : "" : "");
  handleValidate = (product) => {
    let s1 = {...this.state };
    s1.errors.category = this.validateCategory(product.category);
    s1.errors.price = this.validatePrice(product.price);
    s1.errors.brand = this.validateBrand(product.brand,product.category);
    s1.errors.code = this.validateCode(product.code);
    console.log(s1.errors.category)
    this.setState(s1);
    return !(
      s1.errors.code ||
      s1.errors.price ||
      s1.errors.category ||
      s1.errors.brand
    );
  };
  handleSubmit = (e) => {
    e.preventDefault();
    if(this.handleValidate(this.state.product)) {
      this.props.onSubmit(this.state.product);
    }
  };
  textField = (label, name, value, editIndex, err = "") => {
    let disable = editIndex >= 0 && name === "code" ? true : false;
    console.log(err);
    return (
      <React.Fragment>
        <label>{label}</label>
        <input
          type="text"
          className="form-control"
          disabled={disable}
          name={name}
          id={name}
          value={value}
          placeholder={"Enter" + label}
          onChange={this.handleChange}
        />
      
      </React.Fragment>
    );
};
makeRadios = (catArr,name,value,err="") => {
      return(
      <div className="form-check-inline">
    {catArr.map((cat) =>
        <React.Fragment>
          <input
            type="radio"
            className="form-check-input"
            name={name}
            id={name}
            value={cat}
            checked={value === cat}
            onChange={this.handleChange}
          />

          <label className="form-check-label">{cat}</label>
          </React.Fragment>
          )}
          {err ? (
              {err}
          ) : (
              "" )}
              </div>
      )
  }
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    input.type == "checkbox"
    ?s1.product[input.name] = input.checked
    :s1.product[input.name] = input.value
    console.log(s1.product);
    s1.errors={}
    this.setState(s1);
  };
  render() {
    let { code, price, category, brand, limitedStock, specialOffer } =
      this.state.product;
    let { catArr, Food, Apparel, PersonalCare, handleView, editIndex } =
      this.props;
    let { errors } = this.state;
  //  console.log(errors)
    return (
      <React.Fragment>
        {this.textField(
          "Enter Product Code",
          "code",
          code,
          editIndex,
          errors.code
        )}
        {this.textField(
          "Enter Product Price",
          "price",
          price,
          editIndex,
          errors.price
        )}
        <h6>Category</h6>
        {this.makeRadios(catArr,"category",category,errors.category)}
        {category ? (
          <React.Fragment>
            <select
              id="brand"
              name="brand"
              value={brand}
              onChange={this.handleChange}
              className="form-control"
            >
              <option value="" disabled>
                Select the Brand
              </option>
              {category === "Food"
                ? Food.map((cat) => <option>{cat}</option>)
                : category === "Apparel"
                ? Apparel.map((cat) => <option>{cat}</option>)
                : category === "Personal Care"
                ? PersonalCare.map((cat) => <option>{cat}</option>)
                : ""}
              
            </select>
          </React.Fragment>
        ) : (
          ""
        )}
        <h6>Choose another info about the product</h6>
        <div className="form-group">
          <input
            className="form-check-label"
            type="checkbox"
            id="specialOffer"
            name="specialOffer"
            checked={specialOffer}
            value={specialOffer}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Special Offers</label>
        </div>
        <div className="form-group">
          <input
            className="form-check-label"
            type="checkbox"
            id="limitedStock"
            name="limitedStock"
            checked={limitedStock}
            value={limitedStock}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Limited Offers</label>
        </div>

        <button className="btn btn-primary m-2" onClick={this.handleSubmit}>
          {editIndex >= 0 ? "Update" : "Submit"}
        </button>
        <br />
        <button className="btn btn-primary m-2" onClick={() => handleView(0)}>
          Go to HomePage
        </button>
      </React.Fragment>
    );
  }
}
export default AddProduct;


