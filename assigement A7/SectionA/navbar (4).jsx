import React, { Component } from "react";
class NavBar extends Component {
  render() {
    let { Products } = this.props;
    let quantity = Products.reduce((acc,curr) => acc+curr.quantity ,0)
    let value = Products.reduce((acc,curr) => acc+(curr.quantity*curr.price) ,0)
    return (
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">
          Store
        </a>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="#">
               Products<span className="bg-secondary">{Products.length}</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
               Quantity<span className="bg-secondary">{quantity}</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
               Value<span className="bg-secondary">{value}</span>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
export default NavBar;