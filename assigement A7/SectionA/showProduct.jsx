import React, {Component} from "react";
class ShowProduct extends Component{
    render(){
        let {product ,handleView, edit} = this.props;
        return(
            <React.Fragment>
            <div className="row">
            {product.map((pr,index)=>
                <div className="col-3 border bg-light text-center">
                    Code : {pr.code}<br/>
                    Brand : {pr.brand}<br/>
                    Category : {pr.category}<br/>
                    Price : {pr.price}<br/>
                    Quantity : {pr.quantity} <br/>
                    Special offes : {pr.specialOffer ? "Yes" : "No"}<br/>
                    Limited Offers : {pr.limitedStock ? "Yes" : "No"}<br/>
                    <button className="btn btn-warning m-2" onClick={()=> edit(index)}>Edit Details</button>
                </div>
                )}
                </div>
            
                <button className="btn btn-primary m-2" onClick={()=> handleView(1)}>Add New Product</button>
                <button className="btn btn-primary m-2" onClick={()=> handleView(2)}>Receive Stocks</button>
            </React.Fragment>

        )
    }
}
export default ShowProduct;
