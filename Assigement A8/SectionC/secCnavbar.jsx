import React, {Component} from "react";
import {Link} from "react-router-dom"
class NavBar extends Component{
    render(){
      
        return(
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand">
                Billing System
            </a>
            <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                <li class={"nav-item"}>
                    <Link class="nav-link" to="/emps">
                    All
                    </Link>
                </li>
                <li class={"nav-item "}>
                    <Link class="nav-link" to="/emp/NewDelhi">
                    New Delhi
                    </Link>
                </li>
                <li class={"nav-item "}>
                    <Link class="nav-link" to="/emp/Noida">
                    Noida
                    </Link>
                </li>
                </ul>
            </div>
            </nav>
            )
        }
    }
    export default NavBar;