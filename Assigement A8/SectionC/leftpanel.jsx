import React, {Component} from "react";
class LeftPanel extends Component{
    makeCheckBoxes = (arr,name,values,label) => {
        console.log(values,"values")
        return( 
        <React.Fragment>
          <label className="form-check-label">{label}</label>
        {arr.map((opt) => (
            <div className="form-check" key={opt}>
            <input
          type="checkbox"
          className="form-check-input"
          name={name}
          value={opt}
          checked={values.find((val) => val===opt) || false}
          onChange={this.handleChange}
        />
          <label className="form-check-label">{opt}</label>
      </div>
          ))}
          </React.Fragment>
          )
    }
    render(){
        let {Department,Designation,queryParams,city} = this.props;
        let {department="" ,designation=""} = queryParams
        console.log(department,"dept")
        return(
            <React.Fragment>
            <label>Slect the Designation</label>
                {Designation.map((des) => (
                <div className="form-group">
                  <input
                    type="radio"
                    id="designation"
                    name="designation"
                    value={des}
                    // checked={designation == des}
                    onChange={this.handleChange}
                  />
                  <label className="form-check-label">{des}</label>
                </div>
              ))}
            {this.makeCheckBoxes(Department, "department",department.split(","),"Select the Department",)}
              </React.Fragment>
            );
        }
        handleChange = (e) => {
            let { currentTarget: input } = e;
            let queryParams = {...this.props.queryParams};
            console.log(queryParams.department)
            input.type=="checkbox"
            ?queryParams[input.name] = this.updateCBs(input.value,input.checked,queryParams[input.name])
            :queryParams[input.name] = input.value
            this.props.onOptionChange(this.props.city,queryParams);
            console.log(queryParams,"LeftPanel")
         };
          
            
        updateCBs = (value,check,name)=>{
            let inpuArr = name ? name.split(",") : [];
            if(check) inpuArr.push(value)
            else{
                let index = inpuArr.findIndex(ele=>ele===value)
                if(index>=0) inpuArr.splice(index,1)
            }
            return inpuArr.join(",")
        }
}
export default LeftPanel;