import React, {Component} from "react";
class Points extends Component{
    render() {
        let {team} = this.props;
        
        console.log(team)
        return (
            <div className="container">
                <div className="row bg-dark text-center text-white">
                    <div className="col-3" onClick={() => this.sort(0)}>
                        Team
                    </div>
                    <div className="col-1" onClick={() => this.sort(1)}>
                        Played
                    </div>
                    <div className="col-1" onClick={() => this.sort(2)}>
                        Won
                    </div>
                    <div className="col-1" onClick={() => this.sort(3)}>
                        Lost
                    </div>
                    <div className="col-1" onClick={() => this.sort(4)}>
                        Drawn
                    </div>
                    <div className="col-2" onClick={() => this.sort(5)}>
                        Goals For
                    </div>
                    <div className="col-2" onClick={() => this.sort(6)}>
                        Goals Against
                    </div>
                    <div className="col-1" onClick={() => this.sort(7)}>
                        Points
                    </div>
                </div>
                <div className="row bg-light text-center text-dark">
                {team.map(tm=>
                <React.Fragment>
                    <div className="col-3 ">
                        {tm.name}
                    </div>
                    <div className="col-1">
                        {tm.played}
                    </div>
                    <div className="col-1">
                        {tm.won}
                    </div>
                    <div className="col-1">
                        {tm.lost}
                    </div>
                    <div className="col-1">
                        {tm.drawn}   
                    </div>
                    <div className="col-2 ">
                        {tm.goalsFor}
                    </div>
                    <div className="col-2">
                        {tm.goalsAgainst}
                    </div>
                    <div className="col-1">
                        {tm.points}
                    </div>
                </React.Fragment>
                )}
                </div>
            </div>
        )
    }
}
export default Points;