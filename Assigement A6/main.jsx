import React, {Component} from "react";
import AllMatches from "./allmatch";
import NewMatch from "./newmatch";
import Points from "./point";
import {Link, Route} from "react-router-dom"
import Match from "./match"
import NavBar from "./navbar (3)";
class Main extends Component{
    state={
        team: [
            {name: "France", played: 0,won: 0,lost: 0,drawn: 0,goalsFor: 0,goalsAgainst: 0,points: 0},
            {name: "Australia", played: 0,won: 0,lost: 0,drawn: 0,goalsFor: 0,goalsAgainst: 0,points: 0},
            {name: "Brazil", played: 0,won: 0,lost: 0,drawn: 0,goalsFor: 0,goalsAgainst: 0,points: 0},
            {name: "Germany", played: 0,won: 0,lost: 0,drawn: 0,goalsFor: 0,goalsAgainst: 0,points: 0},
            {name: "Argentina", played: 0,won: 0,lost: 0,drawn: 0,goalsFor: 0,goalsAgainst: 0,points: 0},
        ],
        matches: [],
        view: 0,
        teamA: "",
        teamB: "",
        showMatch:0,
    };
    render(){
        let {showMatch,matches,team,view,teamA,teamB} = this.state;
        return(
            <React.Fragment>
            <NavBar matches={matches} />
            {showMatch===0
            ?<React.Fragment>
            <button className="btn btn-primary m-2" onClick={() => this.handleView(1)}>All Matches</button>
            <button className="btn btn-primary m-2" onClick={() => this.handleView(2)}>Points Table</button>
            <button className="btn btn-primary m-2" onClick={() => this.handleView(3)}>New Matches</button><br/>
            </React.Fragment>
            :<Match  teamA={teamA} teamB={teamB} MatchOver={this.MatchOver}/>
            }

            {view===1 
            ? <AllMatches  matches={matches}/>
            : view===2
            ? <Points team={team} matches={matches}/>
            :view===3
            ? <NewMatch team={team} onSubmitA={this.handleSubmitA} onSubmitB={this.handleSubmitB} handleView={this.handleViewB} teamA={teamA} teamB={teamB}/>
            : ""
            }
            
            </React.Fragment>
        )
    }
    MatchOver = (scoreA,scoreB,teamA,teamB) =>{
        let s1 = {...this.state};
        let result;
        if(scoreA>scoreB){
            result= teamA+" won"
        }else if(scoreB>scoreA){
            result= teamB+" won"
        }else{
            result="Match Drawn"
        }
        s1.matches.push({teamA: teamA,teamB: teamB,scoreA: scoreA,scoreB: scoreB,result: result})
        s1.showMatch=0;
        s1.view=0;
        this.UpdatePoints(scoreA,scoreB,teamA,teamB)
        s1.teamA="";
        s1.teamB="";
        console.log(s1)
        this.setState(s1)
    }
    UpdatePoints = (scoreA,scoreB,teamA,teamB) => {
        let s1 = {...this.state};
        let IndexA = s1.team.findIndex(fi=> fi.name===teamA)
        let IndexB = s1.team.findIndex(fi=> fi.name===teamB)
        console.log(IndexA)
        console.log(IndexB)
        let played = s1.team[IndexA].played+1
        let won = scoreA>scoreB ? s1.team[IndexA].won+1 : s1.team[IndexA].won+0
        let lost = scoreA<scoreB ? s1.team[IndexA].lost+1 : s1.team[IndexA].lost+0
        let drawn = scoreA==scoreB ? s1.team[IndexA].drawn+1 : s1.team[IndexA].drawn+0
        let points = s1.team[IndexA].points+(won)+(drawn)

        s1.team.splice(IndexA,1,{name: teamA, played: played,won: won,lost: lost,drawn: drawn,goalsFor: scoreA,goalsAgainst: scoreB,points: points})

        let playedB = s1.team[IndexB].played+1
        let wonB = scoreA<scoreB ? s1.team[IndexB].won+1 : s1.team[IndexB].won+0
        let lostB = scoreA>scoreB ? s1.team[IndexB].lost+1 : s1.team[IndexB].lost+0
        let drawnB = scoreA==scoreB ? s1.team[IndexB].drawn+1 : s1.team[IndexB].drawn+0
        let pointsB = s1.team[IndexB].points+(wonB)+(drawnB)
        
        s1.team.splice(IndexB,1,{name: teamB, played: playedB,won: wonB,lost: lostB,drawn: drawnB,goalsFor: scoreB,goalsAgainst: scoreA,points: pointsB})
        console.log(s1.team)
        this.setState(s1)
    }
    handleSubmitA=(team)=>{
        let s1 = {...this.state};
        s1.teamA=team;
        this.setState(s1)
    }
    handleSubmitB=(team)=>{
        let s1 = {...this.state};
        s1.teamB=team;
        this.setState(s1)
    }
    handleView = (num) => {
        let s1 = {...this.state};
        s1.view=num;
        console.log(s1,"view")
        this.setState(s1)
    }
    handleViewB = (num) => {
        let s1 = {...this.state};
        s1.showMatch=num;
        s1.view=0;
        this.setState(s1)
    }
}
export default Main;