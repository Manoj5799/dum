import React, {Component} from "react";
import {Route} from "react-router-dom"
import Match from "./match"
class NewMatch extends Component{
    handleView = () => 
       
        this.props.handleView(4)
    
    handleSubmitA = (name) =>{
        this.props.onSubmitA(name)
    }
    handleSubmitB = (name) =>{
        this.props.onSubmitB(name)
    }
    render() {
        let {teamA,teamB,team} = this.props;
        return (
           <div className="container">
               <h3 className=" text-center">Team : 1 {teamA}</h3>
               <div className="row">
                <div className="col-1"></div>
               {team.map(tm =>
                <div className="col-2">
                <button className="btn btn-warning m-2" name="teamA" onClick={() => this.handleSubmitA(tm.name)}>{tm.name}</button>
                </div>
                )}
                <div className="col-1"></div>
                </div><br/>
                <h3 className=" text-center">Team : 2 {teamB}</h3>
               <div className="row">
                <div className="col-1"></div>
               {team.map(tm =>
                <div className="col-2">
                <button className="btn btn-warning m-2" onClick={()=>this.handleSubmitB(tm.name)}>{tm.name}</button>
                </div>
                )}
                <div className="col-1"></div>
                </div><br/>
                    <button className="btn btn-dark m-2 text-white" style={{position: "relative",left: "45%"}} 
                    onClick={() => this.handleView()}>Start Match</button>
           </div>
        )
    }
}
export default NewMatch;