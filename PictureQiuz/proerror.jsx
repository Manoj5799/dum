import React, {Component} from "react";
import http from "./httpServicesApi";
class ErrorShow extends Component{
    state ={
        players: {playCode:"",fname:"",name:""},
       edit: false
     };
     handleChange = (e) => {
        const {currentTarget: input} = e;
        let s1 = {...this.state};
        s1.players[input.name] = input.value;
        this.setState(s1) 
    }
    handleSubmit = (e) => {
        e.preventDefault();
        let {players,edit} = this.state;
        console.log(players)
        edit 
        ? this.putData(`/players/${players}`,players)
        : this.postData("/players",players)
        alert("Data Send Database");
    }
    async postData(url,obj) {
        let response = await http.post(url,obj);
        console.log(response);
        this.props.history.push("/players")
    }
    async putData(url,obj){
        let response = await http.put(url,obj);
        console.log(response)
        this.props.history.push("/players")
    }
    handleRetry = () => {
        this.props.handleRetry(this.props.qus+1)
    }
    render() {
        const {fname,name,playCode} = this.state.players;
        let {allAnswer,errors} = this.props;
        return (
            <React.Fragment>
            <h2 className="h1">Welcome to the Picture Quiz : Result </h2>
              {/*  <h3 className="text-center">Player Name : {name}</h3>  */}
                <h3 className="text-center">No of Error:  {errors.length}</h3>
           <br/>
           <div className="container">
           <div className="form-group1">
            <label>Player Code</label>
            <input 
            type="text"
            className="form-control"
            id="playCode"
            name="playCode"
            value={playCode}
            placeholder=" Enter Player Code "
            onChange={this.handleChange}
            />
            </div>
             <div className="form-group1">
            <label>Player Name</label>
            <input 
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={name}
            placeholder="Please Enter Player Nmae "
            onChange={this.handleChange}
            />
            </div>
            <div className="form-group1">
            <label>Player Father Name</label>
            <input 
            type="text"
            className="form-control"
            id="fname"
            name="fname"
            value={fname}
            placeholder="Please Enter Your Father Name"
            onChange={this.handleChange}
            />
            </div>
            <button className="btn btn-primary m-2" onClick={this.handleSubmit}>Submit</button>
            <button className="btn btn-danger m-2" onClick={this.handleRetry}>Retry</button>  
            </div>
            </React.Fragment>
        );
    }
    
}
export default  ErrorShow;