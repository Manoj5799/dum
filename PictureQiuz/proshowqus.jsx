import React, {Component} from "react";
class Questions extends Component{
    state={
        click:0
    }
    change = (an,alpha) => {
       this.props.change(an,alpha)
    }
    render() {
        let {marks,name,qus,ans,change, questions,allAnswer} = this.props;
        let {click} = this.state;
        console.log(this.props.qus)
        return (
            <div classname="a2">
            <React.Fragment>
            <h2 className="h1">Welcome to the Picture Quiz : {name} </h2>
            <div className="containers1">     
            <h5 className="w1">Question No : {qus+1}</h5>
             <img className="img1" src={marks[qus].text} /><br/>
             <div className="btnn">
            <button className="btn btn-primary m-2" onClick={() =>change(marks[qus].options[0],qus,0)}>A : {marks[qus].options[0]}</button>
            <button className="btn btn-primary m-2"  onClick={() =>change(marks[qus].options[1],qus,1)}>B : {marks[qus].options[1]}</button>
            <button className="btn btn-primary m-2" onClick={() =>change(marks[qus].options[2],qus,2)}>C : {marks[qus].options[2]}</button><br/>
            <h3 className="text text-danger">{this.props.error}</h3>
             </div>
             </div>
            </React.Fragment>
            </div>
        );
    }
    
}
export default Questions;