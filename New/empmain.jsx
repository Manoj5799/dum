import React, {Component} from "react";
import { Route,Switch, Redirect } from "react-router-dom";
import Empolyee from "./empolyee";

import AddNewEmp from "./addnewemp";

import NavBar from "./navbar1";
class MainComponent extends Component{ 
    render() {
        return (
            <div className="container">
                <NavBar />
                <Switch>
                <Route path="/emps/add" component={AddNewEmp}   />
                <Route path="/emps/:empCode/edit" component={AddNewEmp}/>
                <Route path="/emps" component={Empolyee}   />
                <Redirect from="/" to="/"   />    
                </Switch>
            </div>
        );
    }
}
export default MainComponent;