import React, { Component } from "react";
import { Link } from "react-router-dom";
import queryString from "query-string";
import http from "./httpServicesApi.js";
import LeftEmp from "./leftemp"
class Empolyee extends Component {
  state = { emps : [],
    departments: ["HR","Finance","Technology","Marketing"],
    designations: ["VP","Manager","Trainee"],
    genders: ["Male","Female"],
   }
   async fetchData() {
    let queryParams = queryString.parse(this.props.location.search)
    let searchStr = this.makeSearchString(queryParams)
    let response = await http.get(`/emps?${searchStr}`);
    console.log("res",response);
    let { data } = response
    console.log("da",data)
    this.setState({emps: data})
}
  async componentDidMount() {
    let response = await http.get("/emps");
    console.log(response);
    let {data} = response;
    console.log("data",data);
    this.setState({emps : data });
  }
  componentDidUpdate(prevProps,prevState){
    if (prevProps!==this.props) this.fetchData();
}  
  

  editEmp=(empCode)=> {
   // let queryParams = queryString.parse(this.props.location.search)
  //  let { empCode } = this.props.match.params
   // let { empCode } = queryParams;
 //console.log("ed",empCode)
 this.props.history.push(`/emps/${empCode}/edit`)
}

  deleteEmp=(index)=>{
    let s1= {...this.state}
    s1.emps.splice(index,1)
    this.setState(s1);

}
callURL = (url,options)=>{
  let searchStr = this.makeSearchString(options)
  this.props.history.push({
      pathname: url,
      search: searchStr,
  })
}
makeSearchString =(options) => {
  let {department,designation,gender}=options
  let searchStr=""
  searchStr = this.addQueryString(searchStr,"department",department)
  searchStr = this.addQueryString(searchStr,"designation",designation)
  searchStr = this.addQueryString(searchStr,"gender",gender)
//  seatchstr = this.addQueryString(seatchstr,"minAge",minAge)
  return searchStr;
}
addQueryString = (str,paramname,paramValue) => 
paramValue ? str ? `${str}&${paramname}=${paramValue}`: `${paramname}=${paramValue}` : str
  render() {
      const { emps} = this.state;
      let  {departments,designations,genders} = this.state;
      let queryParams = queryString.parse(this.props.location.search)
       return (
        <div className="container">
           <div className="row">
                   <div className="col-3">
                        <LeftEmp queryParams={queryParams} departments={departments} designations={designations} genders={genders} onOptionChange={this.handleOptionChange} />
                    </div>   
                  
         <div className="col-9">
                 <h6>List of Empolyee</h6>
                 <div className="row border bg-dark text-white">
                 <div className="col-2 border">EmpCode</div>
                      <div className="col-2 border">Name</div>
                      <div className="col-2 border">Department</div>
                      <div className="col-1 border">Designation</div>
                      <div className="col-1 border">Salary</div>
                      <div className="col-1 border">Gender</div>
                      <div className="col-3 border"></div>
                 </div>
                   
                        {emps.map(pr=>
                         <React.Fragment>
                            <div className="row border">
                        <div className="col-2 border">{pr.empCode}</div>
                        <div className="col-2 border">{pr.name}</div>
                       <div className="col-2 border">{pr.department}</div>
                       <div className="col-1 border">{pr.designation}</div>
                       <div className="col-1 border">{pr.salary}</div>
                        <div className="col-1 border">{pr.gender}</div>
                        <div className="col-3 border">
                        <button className="btn btn-success btn-sm m-2" onClick={()=> this.editEmp(pr.empCode)}>Edit</button>
                         <button className="btn btn-danger btn-sm m-2" onClick={()=> this.deleteEmp()}>Delete</button>
                        </div>
                        </div>
                        </React.Fragment>
                        )}
                   
                      </div>
         
                      </div>
 </div>
       )
 }
 handleOptionChange = (queryParams) => {
  console.log(queryParams)
  this.callURL("/emps",queryParams)
}
}
export default Empolyee;
