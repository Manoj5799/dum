import React, {Component} from "react";
import Questions from "./proshowqus1";
import  ErrorShow from "./proerror1";
class MainComponent extends Component{
    state={
      name:"",
      view: 0,
      questions:[
        {link:"https://images.pexels.com/photos/356378/pexels-photo-356378.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Lion", "Dog", "Cat"],answer:2},
        {link:"https://images.unsplash.com/photo-1552410260-0fd9b577afa6?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8bGlvbnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",options:["Dog","Cat", "Lion"],answer:3},
        {link:"https://images.unsplash.com/photo-1511919884226-fd3cad34687c?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8Y2FyfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",options:["Bike", "Car", "Cycle"],answer:2},
        {link:"https://media.istockphoto.com/photos/bird-blue-macaw-parrot-with-isolated-black-background-picture-id1207011102?b=1&k=20&m=1207011102&s=170667a&w=0&h=Soz0h1pB_drhymeC5UWCNTQuOUrigdIFcMzDLkWGnp0=",options:["Parrot", "Bird","Fish"],answer:1},
        {link:"https://images.unsplash.com/photo-1553284966-19b8815c7817?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8aG9yc2V8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",options:["Fox", "Tiger", "Horse"],answer:3},
        {link:"https://images.unsplash.com/photo-1547082299-de196ea013d6?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fGNvbXB1dGVyfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",options:["Printer", "Computer", "UPS"],answer:2},
         {link:"https://media.istockphoto.com/photos/fox-and-urban-environment-picture-id1301587674?b=1&k=20&m=1301587674&s=170667a&w=0&h=j2kneVjjvR5MykrI2MynVa1HhUtqlgfiECVwy3wDKYQ=",options:["Fox", "Dog", "Cat"],answer:1},
         {link:"https://media.istockphoto.com/photos/we-herd-you-were-looking-for-some-magnificent-cattle-picture-id1303666715?b=1&k=20&m=1303666715&s=170667a&w=0&h=mOQcfUp6wdVwwVtoigfMQZHLGv4RWUzm_5PKvZc58go=",options:["Fox", "Cow", "Cat"],answer:2},
         {link:"https://images.unsplash.com/photo-1518796745738-41048802f99a?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cmFiYml0fGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",options:["Mouse", "Rabbit", "Cat"],answer:2},
         {link:"https://images.unsplash.com/photo-1459682687441-7761439a709d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8ZHVja3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",options:["Duck", "Swan", "Cat"],answer:1},
         {link:"https://media.istockphoto.com/photos/cockerel-and-hens-outdoors-in-summer-meadow-picture-id1225017214?b=1&k=20&m=1225017214&s=170667a&w=0&h=2EhIxaR4mjxFLvyQyY6GOkPTSBBvMxztyDkTCMuu3Zc=",options:["Fox", "Duck", "Hen"],answer:3},
         {link:"https://images.unsplash.com/photo-1516467508483-a7212febe31a?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cGlnfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",options:["Pig", "Dog", "Rabbit"],answer:1},
         {link:"https://media.istockphoto.com/photos/close-up-of-a-donkey-on-a-grassy-mountain-picture-id157482011?b=1&k=20&m=157482011&s=170667a&w=0&h=7yNrjYcz8kgUByi4kN4uRTdALfs9PFmEIiXHzcgMpC0=",options:["Fox", "Donkey", "Cat"],answer:2},
         {link:"https://media.istockphoto.com/photos/goat-picture-id181141852?b=1&k=20&m=181141852&s=170667a&w=0&h=Rpk3O9p3fT0DhW-9OB5j6kgj7V__SqYaC60gB_lDjns=",options:["Fox", "Goat", "Cat"],answer:2},
         {link:"https://media.istockphoto.com/photos/grey-squirrel-very-high-resolution-picture-id90648791?b=1&k=20&m=90648791&s=170667a&w=0&h=fA2fHXo6OAmRbQ3eD9FXWccyuOkb_Q0uWqoZ8TOUZPg=",options:["Squirrel", "Dog", "Cat"],answer:1},
         {link:"https://media.istockphoto.com/photos/monkey-in-a-cage-hugs-a-persons-hand-outside-the-cage-a-mans-hand-picture-id1225564741?b=1&k=20&m=1225564741&s=170667a&w=0&h=1ztLyphFGz9lIGpDqw5PgkFwlyrmS_ZkQicLT7dhnDs=",options:["Monkey", "Donkey", "Cat"],answer:1},
         {link:"https://media.istockphoto.com/photos/brown-bear-with-2-spring-cubs-picture-id1264350242?b=1&k=20&m=1264350242&s=170667a&w=0&h=0HmXnQbLhRs8thWKrssXZ8fKAA9_gW-aitIaLCLpCk8=",options:["Fox", "Bear", "Cat"],answer:2},
         {link:"https://images.unsplash.com/photo-1587455989280-1cef2509a39a?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YmF0fGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",options:["Parrot", "Dog", "Bat"],answer:3},
         {link:"https://images.unsplash.com/photo-1572916396232-b4e9e64da034?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8ZnJvZ3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",options:["Fox", "Frog", "Cat"],answer:2},
         {link:"https://images.unsplash.com/photo-1497671954146-59a89ff626ff?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fGZpc2h8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",options:["Fish", "Snake", "Shark"],answer:1},
    ],
    change:0,
    qus:0,
    answer:"",
    allAnswer:[],
      error: "",
      errors:[],
      random: 0,
    };
    handleChange=(e) =>{
     console.log(e.currentTarget);
     let s1= {...this.state};
     s1.name= e.currentTarget.value;
     console.log(s1.name)
     this.setState(s1);
     

    }
    handleView = () => {
        let s1 = {...this.state};
        s1.view= 1;
        console.log(s1,"view")
        this.setState(s1)
    }
    //an=optins and alpha=Show marks qustion index
    change = (optionAnswer,qus,index,random) => {
        let s1 = {...this.state};
        if(s1.questions[random].answer===index+1){ 
            s1.allAnswer.push(index)
            s1.error=""
            let changeView = s1.qus+1
            s1.qus++
            console.log(changeView,"changeView")
            s1.random = this.findRandom()
            console.log(random)
            if(changeView%5===0) s1.view = 2
            else{
                s1.view = 1
            }
        }
       else{
        s1.errors.push(index)
        s1.error="Wrong Answer"
       }
        this.setState(s1)
    }
   handleRetry = (qusValue) => {
        let s1 = {...this.state}
        s1.qus = qusValue
        s1.view = 1
        s1.errors.splice(0)
        this.setState(s1)
   }
   findRandom = ()=>{
    let random;
    let max = this.state.questions.length;
    random = Math.floor(Math.random() * max) //Finds number between 0 - max
    return random;
}
    render(){
        let {name,view,questions,change,qus,answer,error,errors,allAnswer,random} = this.state;
        console.log(qus)
        

        return (
            view === 0 ? (
            <div className="container">
                <div className="row bg-dark">
                    <div className="col-12 text-center">
                        <h2 className="a1">Welcome to the Home Screen</h2>
                        <div className="form-group">
                             <label>Enter Name</label>
                                 <input 
                                      type="text"
                                 className="form-control"
                                   id="name"
                                   name="name"
                                 value={name}
                                 placeholder="Please enter your name"
                                 onChange={this.handleChange}
                                    />
                        <button className="btn btn-primary m-2" onClick={()=>this.handleView()}>Submit</button>
                   </div>
                  
                    </div>
                </div>
            </div>
        )
        :view === 1 ?  (
         <Questions questions={questions} name={name} qus={qus} error={error} ans={answer} random={random} change={this.change} />  
        
        ):
        view === 2
        ?
        <ErrorShow name={name} errors={errors} qus={qus} allAnswer={allAnswer} handleRetry={this.handleRetry} qus={qus} />
        :""
        )
    }
}
export default MainComponent;