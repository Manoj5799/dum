import React, {Component} from "react";
class Questions extends Component{
    state={
        click:0
    }
    change = (an,alpha) => {
       this.props.change(an,alpha)
    }
    render() {
        let {questions,name,qus,ans,change,allAnswer} = this.props;
        let {click} = this.state;
        console.log(this.props.qus)
        return (
            <div classname="a2">
            <React.Fragment>
            <h2 className="h1">Welcome to the Picture Quiz : {name} </h2>
            <div className="containers1">     
            <h5 className="w1">Question No : {qus+1}</h5>
             <img className="img1" src={questions[qus].link} /><br/>
             <div className="btnn">
            <button className="btn btn-primary m-2" onClick={() =>change(questions[qus].options[0],qus,0)}>A : {questions[qus].options[0]}</button>
            <button className="btn btn-primary m-2"  onClick={() =>change(questions[qus].options[1],qus,1)}>B : {questions[qus].options[1]}</button>
            <button className="btn btn-primary m-2" onClick={() =>change(questions[qus].options[2],qus,2)}>C : {questions[qus].options[2]}</button><br/>
            <h3 className="text text-danger">{this.props.error}</h3>
             </div>
             </div>
            </React.Fragment>
            </div>
        );
    }
    
}
export default Questions;