import React, { Component } from "react";
import {Link} from "react-router-dom";
class NavBar extends Component {
  render() {
   
    return (
      <nav className="navbar navbar-light bg-light">
       <a className="navbar-brand" href="#">
           Navbar {" "}
           <span className="">{this.props.totalCounters}</span>
       </a>
      </nav>
    );
  }
}
export default NavBar;