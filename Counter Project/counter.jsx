import React, { Component } from "react";

 class Counter extends Component {
     state={
         value:this.props.counter.value
     };
   classes() {
    let classes= "bg-";
    classes += this.props.counter.value === 0 ? "warning" : "primary";
    return classes;
 }
     render() {
         console.log(this.classes());
         return (
         <div className="container">
             <button className={this.classes()}>{this.formatCount()}</button>
             <button className="btn btn-secondary btn-sm m-2"
              onClick={() =>this.props.onIncrement(this.props.counter)}>
              Increment
              </button>

             <button className="btn btn-danger btn-sm m-2"
              onClick={() =>this.props.onDelete(this.props.counter.id)}>
             Delete
             </button>
         </div>
         )
     }
   
     formatCount() {
         const { value } = this.props.counter;
       
         return value === 0 ? "Zero" : value;     }
 }
 export default Counter;