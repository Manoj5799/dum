import React, { Component } from "react";
import Counter from "./counter";
import NavBar from "./nav";
 class Counters extends Component {
     state={
         counters:[
             {id:1, value:4},
             {id:2,value:0},
             {id:3,value:0},
             {id:4,value:0}
         ]
     };
     handleIncrement = counter => {
         const counters= [...this.state.counters];
         const index = counters.indexOf(counter);
         counters[index]={...counter};
         counters[index].value++;
         this.setState({counters});
     }
     handelReset= () => {
         const counters=this.state.counters.map(c => {
             c.value=0;
             return c;
         });
         this.setState({counters})
     }
     handelDelete= counterId => {
   console.log("del",counterId);
   const counters = this.state.counters.filter(c => c.id !== counterId);
   this.setState({counters})
     }
     render() {
         return (
           <React.Fragment>
                <NavBar totalCounters={this.state.counters.filter(c =>c.value>0).length}/>
                <div className="container">
              <button className="btn btn-primary btn-sm m-2" onClick={this.handelReset}>Reset</button>
           {this.state.counters.map(counter => (
                <Counter
                 key={counter.id}
                  value={counter.value}
                   id={counter.id}
                    onDelete={this.handelDelete}
                     onIncrement={this.handleIncrement}
                     counter={counter}
                      />
                ))}
               
  
                </div>
         
         </React.Fragment>
         )
     }
    
 }
 export default Counters;